#!/usr/bin/bash

cdate=$(date +%Y-%m-%d)

if [[ ! -z $1 ]]; then
  cdate=$1
fi

rg -N --color never "${cdate}" "/home/rods/develop/playwright/xe.com/dollar.db" \
    | /home/rods/develop/playwright/xe.com/chart.plot \
    && nsxiv /tmp/chart-dollar.png
    # && feh /tmp/chart-dollar.png
