#!/usr/bin/gnuplot -persist

set xdata time
set datafile separator ";"
# 2023-02-02T10:10:11-03:00
set timefmt "%Y-%m-%dT%H:%M:%S"
# show timefmt
set format x "%H:%M"
set grid lc rgb 'white'
# set term qt persist size 1200,700
set term pngcairo size 1200,800 background rgb "gray10"

set border 3 lw 2 lc rgb 'white'
# no legend
set nokey
set xtics nomirror
set ytics nomirror

set xlabel "hours" tc rgb "gray40"
set ylabel "BRL" tc rgb "gray40"

set title "USD to BRL" font ", 25" tc rgb "gray40"

set output '/tmp/chart-dollar.png'

# plot '/dev/stdin' using 1:2 with lines lw 2 lc rgb 'yellow'
plot '/dev/stdin' using 1:2 with linespoints pt 7 ps 0.6  lw 2 lc rgb 'yellow'
