#!/usr/bin/bash

d3="/home/rods/develop/playwright/xe.com/dollar-$(date -d "3 months ago" +%Y%m).db"
d2="/home/rods/develop/playwright/xe.com/dollar-$(date -d "2 months ago" +%Y%m).db"
d1="/home/rods/develop/playwright/xe.com/dollar-$(date -d "1 months ago" +%Y%m).db"
d0="/home/rods/develop/playwright/xe.com/dollar.db"

cat "$d3" "$d2" "$d1" "$d0" \
    | /home/rods/develop/playwright/xe.com/chart-month.plot \
    && nsxiv /tmp/chart-dollar-month.png

