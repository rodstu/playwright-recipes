#! /usr/bin/env bash

export NVM_DIR="/home/rods/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

xe_path="/home/rods/develop/playwright/xe.com"


dunstify_id=0
if [[ $1 == "--manual" ]]; then
    dunstify_id=$(dunstify --printid --timeout 0 '💲 updating...')
fi

value=$(nvm exec --silent 16 node ${xe_path}/index.js --silent)
if [[ ! -z ${value} ]]; then
    last_value=$(tail -n1 ${xe_path}/dollar.db | cut -d\; -f2)

    echo "$(date --iso-8601=seconds);${value}" >> ${xe_path}/dollar.db

    is_up=$(echo "${value} > ${last_value}" | bc -l)

    arrow="-"
    # arrows = 🆙🔻⇑⇓↑↓ 
    if [[ $is_up -eq 1  ]]; then
        arrow=""
    else
        arrow=""
    fi
    echo "${value} > ${last_value} == ${is_up}, ${arrow}"

    # echo "Rods_dollar_change(\"${value}\", \"${arrow}\")" | env DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus awesome-client
    /home/rods/develop/playwright/xe.com/call_awesomewm.sh "$value" "$arrow"

else
    dunstify --timeout 2000 --urgency low "💲 xe.com timeout, 💥 empty [${value}]"
fi

if [[ $1 == "--manual" ]]; then
    dunstify --replace ${dunstify_id} --timeout 2000 --urgency low "💲 updated: ${arrow} ${value}"
    # dunstify --close ${dunstify_id}
fi
