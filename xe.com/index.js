// nvm exec --silent 16 node ~/develop/playwright/xe.com/index.js # debug
// nvm exec --silent 16 node ~/develop/playwright/xe.com/index.js --silent
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const { chromium } = require("playwright");

const launchOptions = {
    headless: true
};

(async () => {
  let browser;
  const vFeedback = !(process.argv?.[2] === "--silent");

  try {
    vFeedback && process.stdout.write("\x1b[90mstart, ");
    browser = await chromium.launch(launchOptions);
    const page = await browser.newPage();

    vFeedback && process.stdout.write("goto, ");
    await page.goto('https://www.xe.com/currencyconverter/convert/?Amount=1&From=USD&To=BRL');

    vFeedback && process.stdout.write("locator\n");

    const element = await page.locator('//*[@id="__next"]/div/div[5]/div[2]/section/div[2]/div/main/div/div[2]/div[1]/div/p[2]').allInnerTexts()
    vFeedback && console.log("🔁🔁🔁 element", element)
    currency = (element || []).join('').replace(' Brazilian Reais', '')

    if (vFeedback) {
      console.log("💰", currency);
    } else {
      console.log(currency.split(" ")?.[0]?.substr(0, 5));
    }
    /* await page.waitForTimeout(3000); */
  } catch(e) {
    console.error("💥", e)
    await exec(`notify-send -u normal 💵  "something is wrong with dollar fetcher\n${e.toString()}"`)
  } finally {
    browser.close()
    // await exec('notify-send -u normal 💵  "everything is good"')
  }
})()
